﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Damas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damas.Tests
{
    [TestClass()]
    public class PiezaTests
    {

        /*[TestMethod()]
        public void PiezaTest()
        {
            Assert.Fail();
        }*/
        [TestMethod()]
        public void moverIzqTest()
        {
            Pieza p = new Pieza(1, 1, true);
            p.moverIzq();

            Assert.IsTrue(p.getX() == 0 && p.getY() == 2);
        }
        [TestMethod()]
        public void moverDerTest()
        {
            Pieza p = new Pieza(1, 1, true);
            p.moverDer();

            Assert.IsTrue(p.getX() == 2 && p.getY() == 2);
        }

    }
}
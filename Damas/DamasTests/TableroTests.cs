﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Damas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damas.Tests
{
    [TestClass()]
    public class TableroTests
    {
        [TestMethod()]
        public void puedeMoverTest()
        {
            Tablero tablero = new Tablero();

            Pieza pieza = new Pieza(7, 3, true);


            //Assert.IsTrue(tablero.puedeMover(pieza) == 0);
            Assert.AreEqual(0, tablero.puedeMover(pieza));
        }

        [TestMethod()]
        public void puedeMoverTest2()
        {
            Tablero tablero = new Tablero();

            Pieza pieza = new Pieza(7, 5, true);


            Assert.IsTrue(tablero.puedeMover(pieza) == 3);
        }
        [TestMethod()]
        public void puedeMoverTest3()
        {
            Tablero tablero = new Tablero();

            Pieza pieza = new Pieza(10, 5, true);


            Assert.IsTrue(tablero.puedeMover(pieza) == 1);
        }

        [TestMethod()]
        public void puedeMoverTest4()
        {
            Tablero tablero = new Tablero();

            Pieza pieza = new Pieza(1, 5, true);

            //Assert.AreEqual(2, tablero.puedeMover(pieza));
            Assert.IsTrue(tablero.puedeMover(pieza) == 2);
        }

        [TestMethod()]
        public void estaOcupadoNegroTest()
        {
            Tablero tablero = new Tablero();

            Assert.IsTrue(tablero.estaOcupadoNegro(2, 10));
        }
    }



}
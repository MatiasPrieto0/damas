﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damas
{
   public class Tablero
    {
        private byte x = 10;
        private byte y = 10;
        private List<Pieza> piezasRojas = new List<Pieza>();
        private List<Pieza> piezasNegras = new List<Pieza>();

        public Tablero()
        {
            for (byte fila = 1; fila < 5; fila++)  
            {
                if (fila % 2 == 0)
                {
                    for (byte col = 2; col < 11; col+=2)
                    {
                        int count = piezasRojas.Count();
                        Pieza p = new Pieza(col, fila, true);
                        piezasRojas.Add(p);
                        Console.WriteLine("Fila: " + piezasRojas[count].getY() + ", Columna: " + piezasRojas[count].getX() + ", Color: " + piezasRojas[count].getColor());
                    }
                }
                if (fila % 2 != 0)
                {
                    for (byte col = 1; col < 11; col+=2)
                    {
                        int count = piezasRojas.Count();
                        Pieza p = new Pieza(col, fila, true);
                        piezasRojas.Add(p);
                        Console.WriteLine("Fila: " + piezasRojas[count].getY() + ", Columna: " + piezasRojas[count].getX() + ", Color: " + piezasRojas[count].getColor());
                    }
                }
            }
            Console.WriteLine("");
            for (byte fila = 10; fila > 6; fila--)
            {
                if (fila % 2 == 0)
                {
                    for (byte col = 2; col < 11; col += 2)
                    {
                        int count = piezasNegras.Count();
                        Pieza p = new Pieza(col, fila, false);
                        piezasNegras.Add(p);
                        Console.WriteLine("Fila: " + piezasNegras[count].getY() + ", Columna: " + piezasNegras[count].getX() + ", Color: " + piezasNegras[count].getColor());
                    }
                }
                if (fila % 2 != 0)
                {
                    for (byte col = 1; col < 11; col += 2)
                    {
                        int count = piezasNegras.Count();
                        Pieza p = new Pieza(col, fila, false);
                        piezasNegras.Add(p);
                        Console.WriteLine("Fila: " + piezasNegras[count].getY() + ", Columna: " + piezasNegras[count].getX() + ", Color: " + piezasNegras[count].getColor());
                    }
                }
            }
        }

        public byte estaBorde(Pieza p)//devuelve 0 si no esta en el borde, 1 si esta a la izquierda, 2 si esta a la derecha
        {
            if (p.getX() == 10)
            {
                return 2;
            }
            if (p.getX() == 1)
            {
                return 1;
            }
            return 0;
        }

        public byte estaTope(Pieza p)//devuelve 0 si no esta en el borde, 1 si esta abajo(0), 2 si esta arriba(10)
        {
            if (p.getY() == this.y)
            {
                return 2;
            }
            if (p.getY() == this.y-(this.y-1))
            {
                return 1;
            }
            return 0;
        }

        public bool estaOcupadoRojo(byte x, byte y)
        {
            foreach(Pieza r in piezasRojas)
            {
                if(r.getX() == x && r.getY() == y)
                {
                    return true;
                }
            }
            return false;
        }
        public bool estaOcupadoNegro(byte x, byte y)
        {
            foreach (Pieza r in piezasNegras)
            {
                if (r.getX() == x && r.getY() == y)
                {
                    return true;
                }
            }
            return false;
        }

        public byte puedeMover(Pieza p)//devuelve 0 si no puede moverse, 1 si solo a la izquierda, 2 si solo a la derecha, 3 si a ambas
        {
            byte xizq;
            byte xder;
            
            if (p.getColor()=="rojo")
            {
                byte yarr = (byte)(p.getY() + 1);
                if (estaTope(p) < 2)
                {
                    if (estaBorde(p) == 0)
                    {
                        xizq = (byte)(p.getX() + 1);
                        xder = (byte)(p.getX() - 1);
                        if(estaOcupadoRojo(xizq, yarr) == true && estaOcupadoRojo(xder, yarr) == true)
                        {
                            return 0;
                        }
                        if (estaOcupadoRojo(xizq, yarr) == false && estaOcupadoRojo(xder, yarr) == false)
                        {
                            return 3;
                        }
                        if (estaOcupadoRojo(xizq, yarr) == true && estaOcupadoRojo(xder, yarr) == false)
                        {
                            return 2;
                        }
                        if (estaOcupadoRojo(xizq, yarr) == false && estaOcupadoRojo(xder, yarr) == true)
                        {
                            return 1;
                        }
                    }
                    if (estaBorde(p) == 1)
                    {
                        xder = (byte)(p.getX() - 1);
                        if (estaOcupadoRojo(xder, yarr) == true)
                        {
                            return 0;
                        }
                        if (estaOcupadoRojo(xder, yarr) == false)
                        {
                            return 2;
                        }
                    }
                    if (estaBorde(p) == 2)
                    {
                        xizq = (byte)(p.getX() + 1);
                        if (estaOcupadoRojo(xizq, yarr) == true)
                        {
                            return 0;
                        }
                        if (estaOcupadoRojo(xizq, yarr) == false)
                        {
                            return 1;
                        }
                    }
                }
                if (estaTope(p) == 2)
                {
                    return 0;
                }
                    
            }
            if (p.getColor() == "negro")
            {
                byte yabj = (byte)(p.getY() - 1);
                if (estaTope(p) > 0)
                {
                    if (estaBorde(p) == 0)
                    {
                        xizq = (byte)(p.getX() + 1);
                        xder = (byte)(p.getX() - 1);
                        if (estaOcupadoNegro(xizq, yabj) == true && estaOcupadoNegro(xder, yabj) == true)
                        {
                            return 0;
                        }
                        if (estaOcupadoNegro(xizq, yabj) == false && estaOcupadoNegro(xder, yabj) == false)
                        {
                            return 3;
                        }
                        if (estaOcupadoNegro(xizq, yabj) == true && estaOcupadoNegro(xder, yabj) == false)
                        {
                            return 2;
                        }
                        if (estaOcupadoNegro(xizq, yabj) == false && estaOcupadoNegro(xder, yabj) == true)
                        {
                            return 1;
                        }
                    }
                    if (estaBorde(p) == 1)
                    {
                        xder = (byte)(p.getX() - 1);
                        if (estaOcupadoNegro(xder, yabj) == true)
                        {
                            return 0;
                        }
                        if (estaOcupadoNegro(xder, yabj) == false)
                        {
                            return 2;
                        }
                    }
                    if (estaBorde(p) == 2)
                    {
                        xizq = (byte)(p.getX() + 1);
                        if (estaOcupadoNegro(xizq, yabj) == true)
                        {
                            return 0;
                        }
                        if (estaOcupadoNegro(xizq, yabj) == false)
                        {
                            return 1;
                        }
                    }
                }
                if (estaTope(p) == 0)
                {
                    return 0;
                }

            }


            return 0;
        }
    }
}

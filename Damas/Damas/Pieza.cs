﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Damas
{
    public class Pieza
    {
        private byte x;
        private byte y;
        private bool color; //true rojo  false negro

        public Pieza(byte x, byte y, bool col)
        {
            this.x = x;
            this.y = y;
            this.color = col;
        }
        public byte getX()
        {
            return x;
        }
        public byte getY()
        {
            return y;
        }
        public string getColor()
        {
            string s = "";
            if (color == true)
            {
                s = "rojo";
            }
            if (color == false)
            {
                s = "negro";
            }
            return s;
        }
        public bool getColorBool()
        {
            return this.color;
        }
        public void moverIzq()
        {
            if (color == true)
            {
                x--;
                y++;
            }
            if (color == false)
            {
                x--;
                y--;
            }
        }
        public void moverDer()
        {
            if (color == true)
            {
                x++;
                y++;
            }
            if (color == false)
            {
                x++;
                y--;
            }
        }
    }
}
